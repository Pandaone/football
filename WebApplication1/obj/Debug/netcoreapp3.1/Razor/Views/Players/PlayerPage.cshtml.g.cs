#pragma checksum "C:\Users\art10\source\repos\WebApplication1\Views\Players\PlayerPage.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "28e852dc91759aae36e55b302152cc77a4e7f153"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Players_PlayerPage), @"mvc.1.0.view", @"/Views/Players/PlayerPage.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\art10\source\repos\WebApplication1\Views\_ViewImports.cshtml"
using WebApplication1.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"28e852dc91759aae36e55b302152cc77a4e7f153", @"/Views/Players/PlayerPage.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cfda4d2356ebed63004dbbc2f0bf5fa5e46326d2", @"/Views/_ViewImports.cshtml")]
    public class Views_Players_PlayerPage : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\art10\source\repos\WebApplication1\Views\Players\PlayerPage.cshtml"
  
    Player player = Model as Player;
    int age = new DateTime((DateTime.Now - player.Age).Ticks).Year - 1;
    ViewData["Title"] = player.Name;

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>");
#nullable restore
#line 7 "C:\Users\art10\source\repos\WebApplication1\Views\Players\PlayerPage.cshtml"
Write(player.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h1>\r\n<img");
            BeginWriteAttribute("src", " src=\"", 185, "\"", 220, 2);
            WriteAttributeValue("", 191, "/img/Players/", 191, 13, true);
#nullable restore
#line 8 "C:\Users\art10\source\repos\WebApplication1\Views\Players\PlayerPage.cshtml"
WriteAttributeValue("", 204, player.ImageUrl, 204, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" height=\"250\" width=\"350\" alt=\"No photo\" />\r\n<p>Date of birth: ");
#nullable restore
#line 9 "C:\Users\art10\source\repos\WebApplication1\Views\Players\PlayerPage.cshtml"
             Write(player.Age.ToString("dddd, dd MMMM yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));

#line default
#line hidden
#nullable disable
            WriteLiteral(" (");
#nullable restore
#line 9 "C:\Users\art10\source\repos\WebApplication1\Views\Players\PlayerPage.cshtml"
                                                                                                                          Write(age);

#line default
#line hidden
#nullable disable
            WriteLiteral(" years old)</p>\r\n<p>Position: ");
#nullable restore
#line 10 "C:\Users\art10\source\repos\WebApplication1\Views\Players\PlayerPage.cshtml"
        Write(player.Position);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n<div>\r\n    ");
#nullable restore
#line 12 "C:\Users\art10\source\repos\WebApplication1\Views\Players\PlayerPage.cshtml"
Write(player.Biography);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</div>\r\n<div>\r\n    ");
#nullable restore
#line 15 "C:\Users\art10\source\repos\WebApplication1\Views\Players\PlayerPage.cshtml"
Write(Html.ActionLink("Team menu", "TeamPage", "Teams", new { id = player.TeamId }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
